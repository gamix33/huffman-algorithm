import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.BitSet;

public class FilesUtils {

    public static String readFile(String fileName){
        String fileContent = "";
        int data;

        try (InputStream in = new FileInputStream(fileName)) {
            while ((data = in.read()) != -1) {
                fileContent += ((char) data);
            }
        } catch (IOException e) {
            System.out.println("FILE READ ERROR " + fileName);
        }

        return fileContent;
    }

    public static void saveBitSetToFile(BitSet bitSet, String fileName){
        try{
            FileOutputStream fos = new FileOutputStream(fileName);
            byte[] byteArray = bitSet.toByteArray();

            fos.write(byteArray);

        } catch (IOException e) {
            System.out.println("FILE SAVING ERROR " + fileName);
        }
    }

    public static BitSet readAsBitSet(String fileName){
        BitSet bitSet = null;
        try{
            byte[] bytes = Files.readAllBytes(Paths.get(fileName));
            bitSet = BitSet.valueOf(bytes);
        } catch (IOException e) {
            System.out.println("FILE READ ERROR " + fileName);
        }
        
        return bitSet;
    }

    public static void saveTextToFile(String text, String fileName){
        try{
            FileOutputStream fos = new FileOutputStream(fileName);
            byte[] byteArray = text.getBytes();

            fos.write(byteArray);

        } catch (IOException e) {
            System.out.println("FILE SAVING ERROR " + fileName);
        }
    }

}
