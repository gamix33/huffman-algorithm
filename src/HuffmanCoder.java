import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;

public class HuffmanCoder {

    public static class TooShortInputDataException extends Throwable{}

    static class SymbolData {
        String symbol;
        int occurrence;

        SymbolData(String symbol, int occurrence){
            this.symbol = symbol;
            this.occurrence = occurrence;
        }

        @Override
        public String toString() {
            return String.format("%s[%d]", symbol != null ? symbol + " " : "", occurrence);
        }
    }

    public static TreeNode huffmanTree(String text, int symLen) throws TooShortInputDataException {
        int len = text.length();
        HashMap<String, Integer> occurencies = new HashMap<>();
        List<TreeNode<SymbolData>> treeList = new ArrayList<>();

        for(int i = 0; i < (len - symLen); i += symLen){
            int to = i + (symLen < len ? symLen : len - 1);
            String s = text.substring(i, to);
            occurencies.put(s, 1 + (occurencies.containsKey(s) ? occurencies.get(s) : 0));
        }

        occurencies.keySet().forEach(sym->{
            treeList.add(new TreeNode<>(new SymbolData(sym, occurencies.get(sym))));
        });

        treeList.forEach(el->System.out.print(el.getData().symbol + " "));
        System.out.print("\n");
        treeList.forEach(el->System.out.print(el.getData().occurrence + " "));

        return buildHuffmanTree(treeList);
    }

    private static TreeNode<SymbolData> buildHuffmanTree(List<TreeNode<SymbolData>> list) throws TooShortInputDataException {
        int len = list.size();
        if(len < 1){
            throw new TooShortInputDataException();
        }

        TreeNode<SymbolData> result;

        if(len > 1){
            TreeNode<SymbolData> u, v;
            if(list.get(0).getData().occurrence < list.get(1).getData().occurrence){
                u = list.get(0);
                v = list.get(1);
            }else{
                u = list.get(1);
                v = list.get(0);
            }

            for(int i = 2; i < len; i++){
                TreeNode<SymbolData> actual = list.get(i);
                if(actual.getData().occurrence < u.getData().occurrence){
                    u = actual;
                }else if(actual.getData().occurrence < v.getData().occurrence){
                    v = actual;
                }
            }

            list.remove(u);
            list.remove(v);

            TreeNode<SymbolData> tn = new TreeNode<>(
                    new SymbolData(null, u.getData().occurrence + v.getData().occurrence)
            );
            tn.addChild(u);
            tn.addChild(v);
            list.add(tn);

            result = buildHuffmanTree(list);
        }else{
            result = list.get(0);
        }

        return result;
    }

    public static BitSet encode(String text, TreeNode huffmanRoot, int symLen){
        BitSet bitSet = new BitSet();
        int j = 0;

        int len = text.length();
        for(int i = 0; i < (len - symLen); i += symLen){
            int to = i + (symLen < len ? symLen : len - 1);
            String s = text.substring(i, to);

            String code = HuffmanCoder.getCode(s, huffmanRoot);
            for(char c : code.toCharArray()){
                if(c == '1'){
                    bitSet.set(j);
                }else{
                    bitSet.clear(j);
                }
                j++;
            }
        }
        return bitSet;
    }

    public static String getCode(String symbol, TreeNode node){
        return getCode(symbol, node, "");
    }

    private static String getCode(String symbol, TreeNode node, String code){
        if(node != null){
            SymbolData data = (SymbolData)node.getData();
            if(data != null && data.symbol != null && data.symbol.equals(symbol)){
                return code;
            } else if(node.getChildren() != null){
                int i = 0;
                for(Object c : node.getChildren()){
                    String _code = getCode(symbol, (TreeNode)c, code + i);
                    if(_code != null)  return _code;
                    ++i;
                }
            }
        }

        return null;
    }

    public static String decode(BitSet bitSet, TreeNode huffmanRoot) {
        String text = "";

        TreeNode node = huffmanRoot;

        for(int i = 0; i < bitSet.length(); i++) {

            if(!bitSet.get(i)) {
                node = (TreeNode) node.getChildren().get(0);
            }else{
                node = (TreeNode) node.getChildren().get(1);
            }

            List<TreeNode> children = node.getChildren();
            if(children == null || children.size() == 0) {
                text += ((SymbolData) node.getData()).symbol;

                node = huffmanRoot;
            }
        }

        return text;
    }

}
