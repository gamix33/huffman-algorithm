import java.io.File;
import java.util.BitSet;

public class Main {

    public static void main(String[] args) {
        String inputFilePath = "seneca.txt";

        String encodedFilePath = "encoded.txt";
        String decodedFilePath = "decoded.txt";

        //String text = "TO BE OR NOT TO BE";
        String text = FilesUtils.readFile(inputFilePath);

        //System.out.println("INPUT: \n" + text);

        TreeNode huffmanRoot;
        try {
            int SYMBOL_LENGTH = 4;

            //buduję drzewo
            huffmanRoot = HuffmanCoder.huffmanTree(text, SYMBOL_LENGTH);

            System.out.println("\n\nHUFFMAN TREE: ");
            huffmanRoot.print();

            //String sampleCode = HuffmanCoder.getCode("B", huffmanRoot);
            //System.out.println(sampleCode);

            //koduję za pomocą wygenerowanego drzewa Huffmana i zapisuję do pliku
            BitSet bitSet = HuffmanCoder.encode(text, huffmanRoot, SYMBOL_LENGTH);
            FilesUtils.saveBitSetToFile(bitSet, encodedFilePath);

            //dekoduję i zapisuję do pliku odkodowany rezultat
            BitSet encodedBitSet = FilesUtils.readAsBitSet(encodedFilePath);
            String decodedContent = HuffmanCoder.decode(encodedBitSet, huffmanRoot);

            //System.out.println("\n\nDECODED CONTENT: ");
            //System.out.println(decodedContent);

            FilesUtils.saveTextToFile(decodedContent, decodedFilePath);

            //statystyki
            File inputFile = new File(inputFilePath);
            File encodedFile = new File(encodedFilePath);
            File decodedFile = new File(decodedFilePath);

            double ratio = (inputFile.length() - encodedFile.length()) / Double.valueOf(inputFile.length());

            System.out.println("RATIO: " + ratio + "\n");
            System.out.println("INPUT FILE SIZE (ORIGINAL): " + inputFile.length() + " bytes");
            System.out.println("ENCODED FILE SIZE: " + encodedFile.length() + " bytes");
            System.out.println("DECODED FILE SIZE: " + decodedFile.length() + " bytes");

        } catch (HuffmanCoder.TooShortInputDataException e) {
            System.out.println("List too short");
        } catch (NullPointerException e){
            System.out.println("List is null");
        }
    }
}
